ThisBuild / scalaVersion := "3.2.0"

lazy val osName: String = sys.props("os.name")
val characterEncoding: String = if (osName.matches(".*indows.*")) "Cp1252" else "UTF8"

scalacOptions ++= Seq(
  "-deprecation"
  , "-encoding", characterEncoding
  , "-feature"
  , "-language:implicitConversions"
  , "-language:higherKinds"
  , "-language:postfixOps"
  , "-unchecked"
)

lazy val commonSettings = Seq(
  fork := true,
  scalaVersion := "3.2.0",
  version := "0.1.0"
)

lazy val utf8test: TaskKey[Unit] = taskKey[Unit]("An example task")

lazy val specificSettings = Seq(
  Compile / run / mainClass := Some("ch.klt.math.run"),
  name := "Ordinals",
  utf8test := {
    println("osName = " + osName)
    println("characterEncoding = " + characterEncoding)
    println("utf8test: α β γ ω ∀ ∃ ∫ ")
  }
)

lazy val root: Project = (project in file(".")).settings(commonSettings: _*).settings(specificSettings: _*)

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.4.4",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "org.scalatest" %% "scalatest-wordspec" % "3.2.14" % "test",
  "org.scalatestplus" %% "scalacheck-1-17" % "3.2.14.0" % "test",
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4"
)
