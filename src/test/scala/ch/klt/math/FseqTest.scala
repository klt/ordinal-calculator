package ch.klt.math

import ch.klt.math
import ch.klt.math.Fseq.*
import org.scalatest.wordspec.AnyWordSpec

import scala.collection.mutable.Stack
import scala.collection.parallel.immutable.ParVector

class FseqTest extends AnyWordSpec with Ordinal {

  // values used to test for equality of returned functions
  private val xs = LazyList.from(0).take(123)

  "Fseq(α).getSeq" when {
    "α = 0" should {
      "return None" in {
        assertResult(None)(Fseq(zero).getSeq)
      }
    }
    "α = ω" should {
      "return int2Ord" in {
        assertResult(xs.map(int2Ord))(xs.map(Fseq(omega).getSeq.get))
      }
    }
    "α = ω²" should {
      "return λn.ω·n" in {
        assertResult(xs.map(omega.mult))(xs.map(Fseq(omegaSquare).getSeq.get))
      }
    }
    "α = ω^ω" should {
      "return λn.ωⁿ" in {
        assertResult(xs.map(int2Ord(_).exp))(xs.map(Fseq(omega.exp).getSeq.get))
      }
    }
  }

}
