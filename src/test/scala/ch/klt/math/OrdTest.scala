package ch.klt.math

import org.scalatest.wordspec.AnyWordSpec

import scala.collection.mutable.Stack
import scala.collection.parallel.immutable.ParVector

class OrdTest extends AnyWordSpec with Ordinal {

  "α.isZero" when {
    "α = 0" should {
      "return true" in {
        assert(zero.isZero)
      }
    }
    "α ≠ 0" should {
      "return false" in {
        val nonZeroOrdinals = List(one, two, omega)
        assert(nonZeroOrdinals.forall(!_.isZero))
      }
    }
  }

  "α.isSuccessor" when {
    "α is a successor ordinal" should {
      "return true" in {
        val sucOrdinals = List(one, two)
        assert(sucOrdinals.forall(_.isSuccessor))
      }
    }
    "α is 0 or a limit ordinal" should {
      "return false" in {
        val nonSucOrdinals = List(zero, omega, omegaStack(2), omegaStack(3))
        assert(nonSucOrdinals.forall(!_.isSuccessor))
      }
    }
  }

  "α.isLimit" when {
    "α is a limit ordinal" should {
      "return true" in {
        val limOrdinals = List(omega, omegaStack(2), omegaStack(3))
        assert(limOrdinals.forall(_.isLimit))
      }
    }
    "α is 0 or a successor ordinal" should {
      "return false" in {
        val nonSucOrdinals = List(zero, one, two, Ord(ParVector(omega, two, zero)))
        assert(nonSucOrdinals.forall(!_.isLimit))
      }
    }
  }

  "α+β" when {
    "β is an ordinal with α < degree(β)" should {
      "return β" in {
        val beta = Ord(ParVector(omegaStack(3), omega, two))
        val smallerOrdinals = List(zero, one, two, int2Ord(42), omega, omegaSquare, omegaStack(2), omegaStack(3))
        assertResult(List.fill(smallerOrdinals.size)(beta))(smallerOrdinals.map(_.add(beta)))
      }
    }
  }

}
