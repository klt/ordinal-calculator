package ch.klt.math

import org.scalatest.wordspec.AnyWordSpec

import scala.collection.mutable.Stack
import scala.collection.parallel.immutable.ParVector

class OrdinalTest extends AnyWordSpec with Ordinal {

  "ordVecIsSorted(vec(α))" when {
    "vec(α) is ordered" should {
      "return true" in {
        val orderedOrdinalVector = List(ParVector(), ParVector(one, zero), ParVector(omega, one, zero))
        assert(orderedOrdinalVector.forall(ordVecIsSorted))
      }
    }
    "vec(α) is not ordered" should {
      "return false" in {
        val unorderedOrdinalVector = List(ParVector(zero, one), ParVector(one, omega, zero))
        assert(unorderedOrdinalVector.forall(v => !ordVecIsSorted(v)))
      }
    }
  }

}
