package ch.klt.math

import ch.klt.math

import scala.annotation.tailrec
import scala.collection.parallel.immutable.ParVector

/**
 * Ordinals are inductively modelled as vectors of ordinals.
 * The empty vector represents 0
 * ParVector(α, β, γ, ...) represents the ordinal ω^α + ω^β + ω^γ + ...
 * Hereby we do not require α ≥ β ≥ γ ≥ ... thus ordinals are not
 * represented in Cantor Normal Form leading that there are infinitely
 * many representations for infinite ordinals.
 * Hence the neceessity for methods equals and normalise
 * @param ords : exponents with base ω
 */
case class Ord(ords: ParVector[Ord]) extends Ordinal with Ordered[Ord] {
  final def getExpVec: ParVector[Ord] = ords

  final override def toString: String = {
    // a beautiful string
    if (isFinite) toBigInt.get.toString else {
      val expStrs: ParVector[String] = ords.map(_.toString).map(omegaExpString)
      val lastInfinite: Int = expStrs.lastIndexWhere(_ != "1")
      // this = α + finiteEnd
      val finiteEnd: Int = expStrs.size - lastInfinite - 1
      (expStrs.take(1 + lastInfinite) ++ Vector.fill[String](finiteEnd.sign)(finiteEnd.toString)).mkString("+")
      //      expStrs.mkString("+")
    }
  }

  final def compare(that: Ord): Int = {
    if (isFinite)
    // if both ordinals finite use bigInt comparison
    // else finite is smaller than infinite
      if (that.isFinite) toBigInt.get compare that.toBigInt.get else -1
    else {
      // this infinite that finite => 1
      if (that.isFinite) 1 else {
        @tailrec
        def go(aN: ParVector[Ord], bN: ParVector[Ord]): Int = {
          //go must be called with exponents of normalised ordinals
          (aN.isEmpty, bN.isEmpty) match {
            case (true, true) => 0
            case (true, false) => -1
            case (false, true) => 1
            case (false, false) =>
              val headComp: Int = aN.head.compare(bN.head)
              if (0 == headComp) go(aN.tail, bN.tail) else headComp
          }
        }
        // we call go with normalised (this,that)
        go(normalise.ords, that.normalise.ords)
      }
    }
  }

  final def equals(that: Ord): Boolean = 0 == compare(that)

  final def isZero: Boolean = this == zero

  final def isSuccessor: Boolean = !isZero && getExpVec.last.isZero

  final def isLimit: Boolean = !isZero && !getExpVec.last.isZero

  final def isExp: Boolean = 1 == getExpVec.size

  final def isFinite: Boolean = getExpVec.forall(_.isZero)

  final def toInt: Option[Int] = if (isFinite) Some(getExpVec.length) else None

  // toInt returns Some(i) iff this is the finite ordinal i
  final def toBigInt: Option[BigInt] = if (isFinite) Some(getExpVec.length) else None
  // toBigInt returns Some(i) iff this is the finite ordinal i

  final def predecessor: Option[Ord] = if (isSuccessor) Some(Ord(getExpVec.init)) else None

  final def successor: Ord = Ord(getExpVec :+ zero)

  final def exp: Ord = Ord(ParVector(this))

  final def normalise: Ord = {
    if (isFinite) this else {
      Ord(ordVecSortedSubVec(getExpVec).map(_.normalise))
    }
  }

  final def isNormal: Boolean = {
    if (isFinite) true
    else {
      val exps = getExpVec
      ordVecIsSorted(exps) && exps.forall(_.isNormal)
    }
  }

  final def degree: Option[Ord] = if (isZero) None else Some(getExpVec.max)

  final def degreePart: Ord = if (isZero) zero else Ord(normalise.getExpVec.filter(_.equals(degree.get)))

  final def nonDegreePart: Ord = if (isZero) zero else Ord(normalise.getExpVec.filterNot(_.equals(degree.get)))

  final def finitePart: Ord = Ord(normalise.getExpVec.filter(_.isZero))

  final def limitPart: Ord = Ord(normalise.getExpVec.filterNot(_.isZero))

  /**
   * Addition using concatenation of exponent vectors and normalisation
   *
   * @param that ordinal to add
   * @return this + that
   */
  final def add(that: Ord): Ord = Ord(getExpVec ++ that.getExpVec).normalise

  /**
   * adds an integer |n| to ordinal this
   *
   * @param n integer |n| to add
   * @return this + that
   */
  final def add(n: Int): Ord = add(int2Ord(n))

  /**
   * multiplies ordinal this with that
   * @param that: ordinal to multiply with
   * @return product this * that
   */
  final def mult(that: Ord): Ord = if (isZero || that.isZero) zero
  else {
    // α * (β + γ) = α*β + α*γ
    // that = thatLimit + thatFinite
    val thisDegree: Ord = degree.get
    val thatFinite: Int = that.finitePart.toInt.get
    val thisTimesThatLimit: Ord = Ord(that.limitPart.getExpVec.map(thisDegree.add))
    if (0 == thatFinite) thisTimesThatLimit else {
      val thisTimesThatFinite: Ord = Ord(ParVector.fill(degreePart.getExpVec.size * thatFinite)(thisDegree)).add(nonDegreePart)
      thisTimesThatLimit.add(thisTimesThatFinite)
    }
  }

  /**
   * multiplies ordinal this with integer |n|
   *
   * @param n: integer to multiply with
   * @return product this * |n|
   */
  final def mult(n: Int): Ord = if (isZero || n == 0) zero
  else {
    val thisDegree: Ord = degree.get
    Ord(ParVector.fill(degreePart.getExpVec.size * n.abs)(thisDegree)).add(nonDegreePart)
  }


  final def fSeq: Fseq = Fseq(this)
}
