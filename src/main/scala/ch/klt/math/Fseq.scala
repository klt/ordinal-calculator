package ch.klt.math

import scala.collection.parallel.immutable.ParVector

/**
 * The case class Fseq represents the fundamental sequence of a limit ordinal λ.
 * The fundamental sequence is a monotone increasing sequence of ordinals
 * having λ as limit.
 * Here we define the Fseq of ω to be int2Ord.
 *
 * @param lambda limit of the fundamental sequence
 */
case class Fseq(lambda: Ord) extends Ordinal {
  final def isLimit: Boolean = lambda.isLimit

  //  final def getLimit: Option[Ord] = if (isLimit) Some(lambda) else None

  /**
   * Returns the fundamental sequence specified if we define int2Ord to be
   * fundamental sequence of ω
   * For other limit ordinals we use the usual recursive definition
   *
   * @return specific fundamental sequence if lambda is a limit ordinal, None otherwise
   */
  final def getSeq: Option[Int => Ord] = if (isLimit) {
    val expVec: ParVector[Ord] = lambda.getExpVec
    val expVecSize: Int = expVec.size
    val expLast: Ord = expVec.last
    if (1 == expVecSize) {
      if (expLast.isZero) Some(n => int2Ord(n)) // λ = ω
      else if (expLast.isSuccessor) { // λ = ω^(α+1)
        Some(expLast.predecessor.get.exp.mult)
      }
      else { // λ = ω^(limit)
        Some(n => Fseq(expLast).getSeq.get(n.abs).exp)
      }
    }
    else { // λ = ω^α + ω^β + ω^γ + ...
      Some(n => Ord(expVec.init).add(Fseq(expVec.last.exp).getSeq.get(n.abs)))
    }
  }
  else None
}
