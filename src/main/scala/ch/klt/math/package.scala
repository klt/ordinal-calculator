package ch.klt

import ch.klt.math.Ord

import scala.collection.parallel.immutable.ParVector

package object math extends Ordinal {
  final val zero: Ord = Ord(ParVector())
  final val one: Ord = zero.exp
  final val two: Ord = Ord(ParVector(zero, zero))
  final val omega: Ord = one.exp
  final val omegaSquare: Ord = two.exp
  final val omegaStr: String = "ω"

}
