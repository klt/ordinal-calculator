package ch.klt.math

import com.typesafe.scalalogging.LazyLogging

import scala.collection.parallel.immutable.ParVector

object SbtRun extends Ordinal with LazyLogging {
  @main def run(): Unit = {
    val alpha: Ord = Ord(ParVector(one, one, int2Ord(9), zero, one, zero, zero))
    val beta: Ord = Ord(ParVector(zero, one, omegaStack(2), Ord(ParVector(omegaStack(2), omegaStack(3))), omegaStack(2), omegaStack(1)))
    val gamma: Ord = Ord(ParVector(zero, one, omega))

    logger.info(s"Java Version: ${System.getProperty("java.version")}")
    logger.info(util.Properties.versionMsg)
    logger.info(s"omegaStack(0)   = ${omegaStack(0)}")
    logger.info(s"omegaStack(1)   = ${omegaStack(1)}")
    logger.info(s"omegaStack(2)   = ${omegaStack(2)}")
    logger.info(s"omegaStack(9)  = ${omegaStack(9)}")
    logger.info(s"omegaStack(-9) = ${omegaStack(-9)}")
    logger.info(s"Ordinal(9).degree    = ${int2Ord(9).degree}")
    logger.info(s"omegaStack(2).degree = ${omegaStack(2).degree}")
    logger.info(s"omegaStack(9).degree = ${omegaStack(9).degree}")
    logger.info(s"α = $alpha")
    logger.info(s"β = $beta")
    logger.info(s"γ = $gamma")
    logger.info(s"α.compare(α) = ${alpha.compare(alpha)}")
    logger.info(s"α.compare(β) = ${alpha.compare(beta)}")
    logger.info(s"α.compare(γ) = ${alpha.compare(gamma)}")
    logger.info(s"β.compare(γ) = ${beta.compare(gamma)}")
    logger.info(s"α.degree = ${alpha.degree}")
    logger.info(s"β.degree = ${beta.degree}")
    logger.info(s"γ.degree = ${gamma.degree}")

  }
}
