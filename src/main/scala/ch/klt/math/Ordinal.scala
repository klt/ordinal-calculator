package ch.klt.math

import ch.klt.math
import ch.klt.math.Ord

import scala.annotation.tailrec
import scala.collection.parallel.CollectionConverters.VectorIsParallelizable
import scala.collection.parallel.immutable.ParVector
import scala.util.Random

trait Ordinal {

  final def int2Ord(n: Int): Ord = Ord(ParVector.fill(n.abs)(zero))

  /**
   * Generates a random ordinal between 0 and omegaStack(N)
   *
   * @param N .
   * @return
   */
  final def generate(N: Int): Ord = {
    def go(N: Int, k: Int, alphas: ParVector[Ord]): ParVector[Ord] = {
      if (0 == N) ParVector.fill(k)(zero) //Ord(alphas))
      else {
        //        val mc: Int = scala.math.pow(2,2.max(7-N)).toInt
        val mc: Int = 3.max(32 - 16 * N)
        val nextK: Int = Random.nextInt(mc).abs.max(Random.nextInt(mc).abs)
        //        logger.info("Ordinal.generate.go (N,k,mc,nextK)")((N,k,mc,nextK))
        ParVector.fill(k)(Ord(go(N - 1, nextK, alphas)))
      }
    }

    go(N, 1, ParVector(zero)).head.normalise
  }

  final def omegaExpString(str: String): String = {
    if (1 == str.length)
      str match {
        case "0" => "1"
        case "1" => omegaStr
        case "2" => omegaStr + "²"
        case "3" => omegaStr + "³"
        case "4" => omegaStr + "⁴"
        case _ => omegaStr + "^" + str
      } else omegaStr + "^(" + str + ")"
  }

  final def ordVecRemoveInitialSmaller(ords: ParVector[Ord]): ParVector[Ord] = {
    if (ords.isEmpty) ords else ords.drop(ords.indexOf(ords.max))
  }

  final def omegaStack(n: Int): Ord = {
    @tailrec
    def go(i: Int, alpha: Ord): Ord = if (i < 1) alpha else go(i - 1, alpha.exp)

    go(n, one)
  }

  final def ordVecIsSorted(ords: ParVector[Ord]): Boolean = ords.sameElements(ords.toVector.sorted.reverse.par)

  final def ordVecSortedSubVec(ords: ParVector[Ord]): ParVector[Ord] = {
    @tailrec
    def go(nVec: ParVector[Ord], aVec: ParVector[Ord]): ParVector[Ord] = {
      if (aVec.isEmpty) nVec else go(nVec :+ aVec.head, ordVecRemoveInitialSmaller(aVec.tail))
    }

    go(ParVector(), ordVecRemoveInitialSmaller(ords))
  }

  final def rec[A](z: A)(succOp: A => A)(limOp: (Int => A) => A)(alpha: Ord): A = {
    if (alpha.isZero) z
    else if (alpha.isSuccessor) succOp(rec[A](z)(succOp)(limOp)(alpha.predecessor.get))
    else limOp(n => rec[A](z)(succOp)(limOp)(Fseq(alpha).getSeq.get(n)))
  }

  // final def recAddition(alpha: Ord)(beta: Ord): Ord = rec[Ord](alpha)(_.successor)(???)(alpha)

}